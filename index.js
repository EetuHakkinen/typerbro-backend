const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const db = require('monk')('mongodb://127.0.0.1:27017/Typerbro');
const cors = require('cors');

app.use(bodyParser.json());
app.use(cors());
app.use(express.static('tyberbro'));

const users = db.get('users');

app.get('/', (req, res) => {
    res.send('<h1>HelloWorld!</h1>');
});

app.get('/api/users', (req, res) => {
    users.find({})
        .then((docs) => {
            console.log('get ok!');
            res.json(docs);
        });
});

app.post('/api/users', (req, res) => {
    const newUserData = req.body;
    users.insert(newUserData)
        .then((docs) => {
            console.log('post ok!');
            res.json(newUserData);
        }).catch((e) => {
            console.log('error while posting data, ', e);
        })
});

app.post('/api/signIn', (req, res) => {
    const idToken = req.body;
    console.log('Sign-in: ', idToken);
    res.json({res:['user signed in!', idToken]});
})

app.delete('/api/users', (req, res) => {
    users.findOneAndDelete(req.body)
        .then((doc) => {
            console.log(req.body, ' removed');
            res.json(doc);
        })
        .catch((e) => {
            console.log('Error while removing user, ', e);
            res.json(e);
        });
})

const PORT = 80;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});

db.then(() => {
    console.log('Connected correctly to database');
});
